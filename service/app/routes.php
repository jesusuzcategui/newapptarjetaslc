<?php
declare(strict_types = 1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use App\Application\Actions\Webpay\InitTransactionAction;
use App\Application\Actions\Webpay\CreatePaymentAction;
use App\Application\Actions\Webpay\ReturnPageAction;
use App\Application\Actions\Tarjetas\GetAviableTarjetasAction;
use App\Application\Actions\Sendiblue\SendemailAction;
use App\Application\Actions\Sendiblue\SendContactAction;
use App\Application\Actions\Webpay\GetOrderAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()
            ->write(json_encode($_ENV));
        return $response;
    });

    $app->group('/tarjetas', function (Group $group) {
        $group->get('/list', GetAviableTarjetasAction::class);
    });

    $app->group('/webpay', function (Group $group) {
        $group->get('/createPayment', CreatePaymentAction::class);
        $group->post('/initTransaction', InitTransactionAction::class);
        $group->map([
            'GET',
            'POST'
        ], '/returnPage', ReturnPageAction::class);
        $group->get('/getResult', GetOrderAction::class);
    });

    $app->group('/test', function(Group $group){
        $group->get('/sendiblue/{case}', SendemailAction::class);
    });

    $app->group('/utils', function(Group $group){
        $group->post('/sendContact/{template}', SendContactAction::class);
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });
};
