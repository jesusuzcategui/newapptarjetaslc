<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Psr\Container\ContainerInterface;
use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;

abstract class WebpayAction extends Action
{

    public function __construct(LoggerInterface $logger, ContainerInterface $container){
        parent::__construct($logger, $container);
    }
}
