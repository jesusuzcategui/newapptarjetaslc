<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Exception;
use Psr\Log\LoggerInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use PDO;

class GetOrderAction extends WebpayAction 
{
    public function __construct(LoggerInterface $logger, ContainerInterface $container) {
        parent::__construct($logger, $container);
    }

    protected function action(): Response
    {
        try {
            $queryParams = $this->request->getQueryParams();
            $orderId = $queryParams['orderId'];

            if(empty($orderId)){
                return $this->respondWithData(false)->withStatus(406); 
            }

            $queryPin = <<<SQL
                SELECT tar.id, tar.pin, mon.monto, ven.correo_cliente, ven.telefono, ven.inicio, ven.estado, ven.mensaje_webpay, ven.id_operacion FROM ventas_frecuentes AS ven LEFT JOIN targetas AS tar ON tar.id = ven.id_targeta LEFT JOIN monto AS mon ON mon.id = tar.precio WHERE ven.id = '{$orderId}'
                SQL;
                $resultPin = $this->database->query($queryPin)->fetch(PDO::FETCH_OBJ);

            $aviableErrors = [
                "0"  => "Venta procesada",
                "-1" => "Invalid card (Tarjeta invalida)",
                "-2" => "Connection error (Error de conexión)",
                "-3" => "Exceeds maximum amount (Excede el monto maximo)",
                "-4" => "Invalid expiration date (Fecha de expiracion invalida)",
                "-5" => "Authentication problem (Problema en la autenticacion)",
                "-6" => "General rejection (Rechazo general)",
                "-7" => "Locked card (Tarjeta bloqueada)",
                "-8" => "Expired card (Tarjeta expirada)",
                "-9" => "Transaction not supported (Transaccion no soportada)",
                "-10" => "Transaction problem (Transaccion con problemas)",
            ];

            $resultPin->decodeResult = ($resultPin->mensaje_webpay == "N/A") ? null : json_decode($resultPin->mensaje_webpay);

            $resultPin->reasonFailed = empty($resultPin->decodeResult) ? null : $aviableErrors[strval($resultPin->decodeResult->responseCode)];
            
            return $this->respondWithData($resultPin);
                
        } catch (Exception $e){

            return $this->respondWithData($e->getMessage())->withStatus(400);
        }
    }
}