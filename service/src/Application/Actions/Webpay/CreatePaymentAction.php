<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class CreatePaymentAction extends WebpayAction
{
    /**
     * {@inheritdoc}
     */

    public function __construct(LoggerInterface $logger, ContainerInterface $container) {
        parent::__construct($logger, $container);
        WebpayPlus::configureForTesting();
    }

    protected function action(): Response
    {
        try {

            return $this->respondWithData($this->container->get('database'));

        } catch(Exception $e) {
            return $this->respondWithData([
                "msg" => $e->getMessage(),
                "code" => $e->getCode()
            ])->withStatus(400);
        }

        return $this->respondWithData(["Error"]);
    }
}
