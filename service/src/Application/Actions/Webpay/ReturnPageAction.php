<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Exception;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use \PDO;

use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;
use SendinBlue\Client\Configuration AS SendinBlueConf;
use SendinBlue\Client\Api\TransactionalEmailsApi;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;
use SendinBlue\Client\Model\SendSmtpEmail;

class ReturnPageAction extends WebpayAction
{
    public $sendinBlueApi;
    public $sendinBlueConfig;
    public $sendinBlueSmtp;
    public $strError;

    public function __construct(LoggerInterface $logger, ContainerInterface $container) {
        parent::__construct($logger, $container);
        WebpayPlus::configureForTesting();

        $this->sendinBlueConfig = SendinBlueConf::getDefaultConfiguration()->setApiKey('api-key', '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');
        $this->sendinBlueApi = new TransactionalEmailsApi(
            new Client(),
            $this->sendinBlueConfig
        );
        $this->sendinBlueSmtp = new SendSmtpEmail;

        $this->sendinBlueSmtp['subject'] = "Compra de Tarjeta Locutorios";
        $this->sendinBlueSmtp['sender'] = array("name" => "Tarjeta Locutorios", "email" => "contacto@tarjetalocutorios.com");

        $this->strError = [
            "-1" => "Invalid card (Tarjeta invalida)",
            "-2" => "Connection error (Error de conexión)",
            "-3" => "Exceeds maximum amount (Excede el monto maximo)",
            "-4" => "Invalid expiration date (Fecha de expiracion invalida)",
            "-5" => "Authentication problem (Problema en la autenticacion)",
            "-6" => "General rejection (Rechazo general)",
            "-7" => "Locked card (Tarjeta bloqueada)",
            "-8" => "Expired card (Tarjeta expirada)",
            "-9" => "Transaction not supported (Transaccion no soportada)",
            "-10" => "Transaction problem (Transaccion con problemas)",
        ];
    }

    protected function action(): Response
    {
        try {

            $queryParams = (array) $this->request->getQueryParams();
            
            $domainApp = $this->container->get('domainPath');
            $errorPage = $domainApp . "500";
            $successPage = $domainApp . "validate";
            $faildPage = $domainApp . "validate";

            if(empty($queryParams["token_ws"])){

                if(empty($_REQUEST["TBK_TOKEN"])){
                    $this->logger->info("Transaccion declinada por sistema ya que el usuario tardo mas de 5 minutos en la transaccion sin proceder: {$_REQUEST["TBK_ORDEN_COMPRA"]} y ID session: {$_REQUEST["TBK_ID_SESION"]}");
                
                    $queryPin = <<<SQL
                    SELECT tar.id, tar.pin, mon.monto, ven.correo_cliente, ven.inicio FROM ventas_frecuentes AS ven LEFT JOIN targetas AS tar ON tar.id = ven.id_targeta LEFT JOIN monto AS mon ON mon.id = tar.precio WHERE ven.id_operacion = '{$_REQUEST["TBK_ORDEN_COMPRA"]}'
                    SQL;
                    
                    $resultPin = $this->database->query($queryPin)->fetch(PDO::FETCH_OBJ);
                    
                    if(is_bool($resultPin)){
                        $this->logger->info("Error en base de datos para trasaccion: {$_REQUEST["TBK_ORDEN_COMPRA"]} {json_encode($this->database->error)}");
                        return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                    }                
                    
                    $updateSale = $this->database->update('ventas_frecuentes', ['estado' => 5, 'fin' => date("Y-m-d H:i:s", time() - 3600)], ['id_operacion' => $_REQUEST["TBK_ORDEN_COMPRA"]]);

                    $updateCard = $this->database->update('targetas', ['estado_id' => 1], ['id' => $resultPin->id]);               
                    
                    if($updateSale->rowCount() == 0 || $updateCard->rowCount() == 0){
                        $errorDb = json_encode($this->database->error);
                        $this->logger->info("Error en base de datos para trasaccion: {$_REQUEST["TBK_ORDEN_COMPRA"]} {$errorDb}");
                        return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                    }

                    $this->logger->info("Venta y tarjeta actualizadas");
                    return $this->response->withHeader('Location', $faildPage)->withStatus(301);
                }

                $this->logger->info("Transaccion declinada por usuario con numero: {$_REQUEST["TBK_ORDEN_COMPRA"]} y token {$_REQUEST["TBK_TOKEN"]} y ID session: {$_REQUEST["TBK_ID_SESION"]}");
                
                $queryPin = <<<SQL
                SELECT tar.id, tar.pin, mon.monto, ven.correo_cliente, ven.inicio FROM ventas_frecuentes AS ven LEFT JOIN targetas AS tar ON tar.id = ven.id_targeta LEFT JOIN monto AS mon ON mon.id = tar.precio WHERE ven.id_operacion = '{$_REQUEST["TBK_ORDEN_COMPRA"]}'
                SQL;
                
                $resultPin = $this->database->query($queryPin)->fetch(PDO::FETCH_OBJ);
                
                if(is_bool($resultPin)){
                    $this->logger->info("Error en base de datos para trasaccion: {$_REQUEST["TBK_ORDEN_COMPRA"]} {json_encode($this->database->error)}");
                    return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                }                
                
                $updateSale = $this->database->update('ventas_frecuentes', ['estado' => 5, 'fin' => date("Y-m-d H:i:s", time() - 3600)], ['id_operacion' => $_REQUEST["TBK_ORDEN_COMPRA"]]);

                $updateCard = $this->database->update('targetas', ['estado_id' => 1], ['id' => $resultPin->id]);               
                
                if($updateSale->rowCount() == 0 || $updateCard->rowCount() == 0){
                    $errorDb = json_encode($this->database->error);
                    $this->logger->info("Error en base de datos para trasaccion: {$_REQUEST["TBK_ORDEN_COMPRA"]} {$errorDb}");
                    return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                }

                $this->logger->info("Venta y tarjeta actualizadas");
                return $this->response->withHeader('Location', $faildPage)->withStatus(301);
            }

            $transaction = (new Transaction)->commit($queryParams["token_ws"]);
            
            if( $transaction->isApproved() ){
                $this->logger->info("La compra con ID {$transaction->buyOrder} realizada el {$transaction->transactionDate} tuvo una respuesta: {$transaction->responseCode} y un estatus {$transaction->status} y su VCI fue {$transaction->vci}");
                
                $queryPin = <<<SQL
                SELECT tar.id, tar.pin, mon.monto, ven.correo_cliente, ven.inicio FROM ventas_frecuentes AS ven LEFT JOIN targetas AS tar ON tar.id = ven.id_targeta LEFT JOIN monto AS mon ON mon.id = tar.precio WHERE ven.id_operacion = '{$transaction->buyOrder}'
                SQL;
                
                $resultPin = $this->database->query($queryPin)->fetch(PDO::FETCH_OBJ);
                
                if(is_bool($resultPin)){
                    $this->logger->info("Error en base de datos para trasaccion: {$transaction->buyOrder} {json_encode($this->database->error)}");
                    return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                }                
                
                $updateSale = $this->database->update('ventas_frecuentes', ['estado' => 3, 'mensaje_webpay' => json_encode($transaction), 'fin' => date("Y-m-d H:i:s", time() - 3600)], ['id_operacion' => $transaction->buyOrder]);

                $updateCard = $this->database->update('targetas', ['estado_id' => 3], ['id' => $resultPin->id]);               
                
                if($updateSale->rowCount() == 0 || $updateCard->rowCount() == 0){
                    $errorDb = json_encode($this->database->error);
                    $this->logger->info("Error en base de datos para trasaccion: {$transaction->buyOrder} {$errorDb}");
                    return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                }

                $this->logger->info("Venta y tarjeta actualizadas");

                $this->sendinBlueSmtp['templateId'] = 12;

                $this->sendinBlueSmtp['to'] = array(
                    array(
                        "email" => $resultPin->correo_cliente,
                        "name" => $resultPin->correo_cliente
                    )
                );
        
                $this->sendinBlueSmtp['params'] = [
                    "NUMEROCOMPRA" => $transaction->buyOrder,
                    "PRECIOCOMPRA" => "CLP " . $resultPin->monto,
                    "FECHACOMPRA" => $resultPin->inicio,
                    "PINCOMPRA" => $resultPin->pin
                ];

                $this->sendinBlueApi->sendTransacEmail($this->sendinBlueSmtp);

                $this->logger->info("Correo enviado");
                $this->logger->info("Redireccionando a cliente a pagina de validacion.");

                return $this->response->withHeader('Location', $successPage)->withStatus(301);
                
            } else {

                $responseCode = strval($transaction->responseCode);

                $this->logger->info("La compra con ID {$transaction->buyOrder} realizada el {$transaction->transactionDate} tuvo una respuesta: {$transaction->responseCode} y un estatus {$transaction->status} y su VCI fue {$transaction->vci}");
                
                $queryPin = <<<SQL
                SELECT tar.id, tar.pin, mon.monto, ven.correo_cliente, ven.telefono, ven.inicio FROM ventas_frecuentes AS ven LEFT JOIN targetas AS tar ON tar.id = ven.id_targeta LEFT JOIN monto AS mon ON mon.id = tar.precio WHERE ven.id_operacion = '{$transaction->buyOrder}'
                SQL;
                
                $resultPin = $this->database->query($queryPin)->fetch(PDO::FETCH_OBJ);
                
                if(is_bool($resultPin)){
                    $this->logger->info("Error en base de datos para trasaccion: {$transaction->buyOrder} {json_encode($this->database->error)}");
                    return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                }                
                
                $updateSale = $this->database->update('ventas_frecuentes', ['estado' => 5, 'mensaje_webpay' => json_encode($transaction), 'fin' => date("Y-m-d H:i:s", time() - 3600)], ['id_operacion' => $transaction->buyOrder]);

                $updateCard = $this->database->update('targetas', ['estado_id' => 1], ['id' => $resultPin->id]);               
                
                if($updateSale->rowCount() == 0 || $updateCard->rowCount() == 0){
                    $errorDb = json_encode($this->database->error);
                    $this->logger->info("Error en base de datos para trasaccion: {$transaction->buyOrder} {$errorDb}");
                    return $this->response->withHeader('Location', $errorPage)->withStatus(301);
                }

                $this->logger->info("Venta y tarjeta actualizadas");

                $this->sendinBlueSmtp['templateId'] = 13;

                $this->sendinBlueSmtp['to'] = array(
                    array(
                        "email" => "uzcateguijesusdev@gmail.com",
                        "name" => "uzcateguijesusdev@gmail.com"
                    )
                );
        
                $this->sendinBlueSmtp['params'] = [
                    "NUMEROCOMPRA" => $transaction->buyOrder,
                    "PRECIOCOMPRA" => "CLP " . $resultPin->monto,
                    "FECHACOMPRA" => $resultPin->inicio,
                    "PINCOMPRA" => $resultPin->pin,
                    "CORREOCLIENTE" => $resultPin->correo_cliente,
                    "TELEFONOCLIENTE" => $resultPin->telefono,
                    "WEBPAYREASON" => $this->strError[$responseCode]
                ];

                $this->sendinBlueApi->sendTransacEmail($this->sendinBlueSmtp);

                return $this->response->withHeader('Location', $faildPage)->withStatus(301);

            }

        } catch(Exception $e) {
            $this->logger->error("Ha ocurrido un error en el servidor {$e->getCode()} con detalle: {$e->getMessage()}");
            return $this->response->withHeader('Location', $errorPage)->withStatus(301);
        }
    }
}
