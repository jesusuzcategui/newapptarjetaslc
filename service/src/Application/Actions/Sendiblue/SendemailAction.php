<?php

declare(strict_types=1);

namespace App\Application\Actions\Sendiblue;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;
use Psr\Log\LoggerInterface;
use Psr\Container\ContainerInterface;
use \PDO;

use SendinBlue\Client\Configuration AS SendinBlueConf;
use SendinBlue\Client\Api\TransactionalEmailsApi;
use GuzzleHttp\Client;
use SendinBlue\Client\Model\SendSmtpEmail;

use App\Application\Helpers\SendinBlue;


class SendemailAction extends SendiblueAction
{
    /**
     * {@inheritdoc}
     */

    public $sendinBlueApi;
    public $sendinBlueConfig;
    public $sendinBlueSmtp;

    public function __construct(LoggerInterface $logger, ContainerInterface $container) {
        parent::__construct($logger, $container);
        
        WebpayPlus::configureForTesting();
        $this->sendinBlueConfig = SendinBlueConf::getDefaultConfiguration()->setApiKey('api-key', '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');
        $this->sendinBlueApi = new TransactionalEmailsApi(
            new Client(),
            $this->sendinBlueConfig
        );
        $this->sendinBlueSmtp = new SendSmtpEmail;

        $this->sendinBlueSmtp['subject'] = "Prueba sendinblue desde servicio nuevo";
        $this->sendinBlueSmtp['sender'] = array("name" => "Tarjeta Locutorios", "email" => "contacto@tarjetalocutorios.com");
        $this->sendinBlueSmtp['to'] = array(
            array("email" => "clarenas@locutorios.cl", "name" => "Cristian Larenas")
        );
        $this->sendinBlueSmtp['templateId'] = 12;
        $this->sendinBlueSmtp['params'] = [
            "NUMEROCOMPRA" => "111111111",
            "PRECIOCOMPRA" => "CLP 1000",
            "FECHACOMPRA" => "12/02/2022",
            "PINCOMPRA" => "1236598642"
        ];
    }

    protected function action(): Response
    {
        try {
            return $this->respondWithData($this->args['case']);
        } catch(Exception $e){
            return $this->respondWithData($e->getMessage());
        }
    }
}
