<?php

declare(strict_types=1);

namespace App\Application\Actions\Sendiblue;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;
use Psr\Log\LoggerInterface;
use Psr\Container\ContainerInterface;
use \PDO;

use SendinBlue\Client\Configuration AS SendinBlueConf;
use SendinBlue\Client\Api\TransactionalEmailsApi;
use GuzzleHttp\Client;
use SendinBlue\Client\Model\SendSmtpEmail;

use App\Application\Helpers\SendinBlue;


class SendContactAction extends SendiblueAction
{
    /**
     * {@inheritdoc}
     */

    public $sendinBlueApi;
    public $sendinBlueConfig;
    public $sendinBlueSmtp;

    public function __construct(LoggerInterface $logger, ContainerInterface $container) {
        parent::__construct($logger, $container);
        
        WebpayPlus::configureForTesting();

        $this->sendinBlueConfig = SendinBlueConf::getDefaultConfiguration()->setApiKey('api-key', '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');
        $this->sendinBlueApi = new TransactionalEmailsApi(
            new Client(),
            $this->sendinBlueConfig
        );
        $this->sendinBlueSmtp = new SendSmtpEmail;

        $this->sendinBlueSmtp['sender'] = array("name" => "Tarjeta Locutorios", "email" => "contacto@tarjetalocutorios.com");
    }

    protected function action(): Response
    {
        try {
            $contactData = (object) $this->request->getParsedBody();

            $this->sendinBlueSmtp['subject'] = ($this->args['template'] == "14") ? "Contacto de Tarjeta Locutorios" : "Quiero ser distribuidor Tarjeta Locutorios"; 

            $this->sendinBlueSmtp['templateId'] = (int) $this->args['template'];

            $this->sendinBlueSmtp['to'] = array(
                array(
                    "email" => "abonos@locutorios.cl",
                    "name" => "Abonos locutorios"
                )
            );
    
            $this->sendinBlueSmtp['params'] = [
                "CONTACTONOMBRE" => $contactData->nombre,
                "CONTACTOPHONE" => $contactData->telefono,
                "CONTACTOEMAIL" => $contactData->email,
                "CONTACTOMENSAJE" => $contactData->mensaje
            ];

            $this->sendinBlueApi->sendTransacEmail($this->sendinBlueSmtp);

            return $this->respondWithData(["mensaje" => "Mensaje enviado"]);
        } catch(Exception $e){
            return $this->respondWithData($e->getMessage());
        }
    }
}
