import VueRouter from 'vue-router';

import IndexPage from './App/Pages/Index.vue';
import CartPage from './App/Pages/Cart.vue';
import WebapyPage from './App/Pages/Webpay.vue';
import ProccessPage from './App/Pages/Process.vue';
import SuccessPage from './App/Pages/SuccessSale.vue';
import FailPage from './App/Pages/FailSale.vue';
import ErrorServerPage from './App/Pages/ErrorServer.vue';
import Error404Page from './App/Pages/Error.vue';
import ContactPage from './App/Pages/Contact.vue';

const routes = [
    {
        path: "/",
        component: IndexPage,
        name: "Home"
    },
    {
        path: "/cart",
        component: CartPage,
        name: "Cart"
    },
    {
        path: "/webpay",
        component: WebapyPage,
        name: "Webpay"
    },
    {
        path: "/validate",
        component: ProccessPage,
        name: "Procesando"
    },
    {
        path: "/exito-compra",
        component: SuccessPage,
        name: "Compra exitosa"
    },
    {
        path: "/error-compra",
        component: FailPage,
        name: "Compra errada"
    },
    {
        path: "/500",
        component: ErrorServerPage,
        name: "Error faltal"
    },
    {
        path: "/404",
        component: Error404Page,
        name: "Página no encontrada"
    },
    {
        path: "/contacto",
        component: ContactPage,
        name: "Contact"
    },
    {
        path: "/quiero-ser-distribuidor",
        component: ContactPage,
        name: "Ser-Distribuidor"
    },
    {
        path: "*",
        beforeEnter: (to, from, next) => next('/404')
    }
];

const router = new VueRouter({
    mode: "history",
    routes: routes,
    scrollBehavior: (to) => {
        if(to.hash){
            return {
                selector: to.hash,
                behavior: 'smooth',
            }
        } else {
            return {
                selector: 'body',
                behavior: 'smooth'
            }
        }
    }
});

export default router;