import Vue from 'vue';
import Vuex from 'vuex';
import { setWebpay, setTarjetas, setCart, setSale } from './App/Vuex/mutation';
import { setWebpayAction, setTarjetasAction, setCartAction, getResultSaleAction } from './App/Vuex/actions';
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        webpay: null,
        tarjetas: [],
        cart: {idprice: null, price: null, email: null, phone: null},
        sale: null
    },
    mutations: {
        setWebpay,
        setTarjetas,
        setCart,
        setSale
    },
    actions: {
        setWebpayAction,
        setTarjetasAction,
        setCartAction,
        getResultSaleAction
    },
    getters: {
        getWebpay(state){
            return state.webpay;
        },
        getTarjetas( state ){
            return state.tarjetas;
        },
        getCart( state ){
            return state.cart;
        },
        getTarjeta(state){
            return (id) => {
                return state.tarjetas.find(card => card.idprecio === id);
            }
        },
        getSale(state){
            return state.sale;
        }
    }
});