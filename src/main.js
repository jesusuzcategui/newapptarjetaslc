import Vue from 'vue';
import App from './App/App.vue';
import VueRouter from 'vue-router';
import VueCookies  from 'vue-cookies';
import Vuetify from 'vuetify';
import VueMobileDetection from 'vue-mobile-detection';
import VueCountdown from '@chenfengyuan/vue-countdown';
import VueMeta from 'vue-meta';
import { store } from './store';
import router from './router';
import 'vuetify/dist/vuetify.css';
import './sass/App.scss';

Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VueMeta, {
    refreshOnceOnNavigation: true
});
Vue.use(VueCookies);
Vue.use(VueMobileDetection);
Vue.component(VueCountdown.name, VueCountdown);

const VuetifyOptions = {
    theme: {
        themes: {
            light: {
                primary: "#19357D",
                secondary: "#FDDC1E",
                accent: "#FFFFFF"
            }
        }
    }
};

new Vue({
  el: '#app',
  router: router,
  vuetify: new Vuetify(VuetifyOptions),
  store: store,
  render: h => h(App),
});