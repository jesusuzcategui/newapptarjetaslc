const setWebpay = (state, data) => {
    state.webpay = data;
};

const setTarjetas = (state, data) => {
    state.tarjetas = data;
};

const setCart = (state, data) => {
    state.cart = data;
};

const setSale = (state, data) => {
    state.sale = data;
};

export { setWebpay, setTarjetas, setCart, setSale };