import axios from 'axios';
import { APISERVICE } from '../enviroment';

async function sendContactForm(to, name, email, phone, message) {
    try {
        const form = {
            nombre: name,
            telefono: phone,
            email: email,
            mensaje: message
        };

        const { status } = await axios.post(`${APISERVICE}utils/sendContact/${to}`, form);

        return true;
        
    } catch (error){
        console.error(error);
        return false;
    }
}

export { sendContactForm };