import axios from 'axios';
import { APISERVICE } from '../enviroment';

async function findCardAviables(){
    let resp = null;

    const { data, status} = await axios.get(`${APISERVICE}tarjetas/list`);
    if(status === 200){
        resp = data.data;
    } else {
        resp = null;
    }

    return resp;
}

export { findCardAviables };